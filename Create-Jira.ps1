Param(
  [string]$component,
  [string]$newVersion,
  [string]$buildTypeId,
  [string]$tcUserName,
  [string]$tcPassword,
  [string]$tcHostName,
  [string]$jiraUsername,
  [string]$jiraPassword,
  [string]$jiraHost,
  [string]$jiraProjectKey,
  [string]$tpAccessToken,
  [string]$impactStatement
)

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function Get-CommitMessages
{
	$lastChange = Get-LastChange
	
	$uri = "$($tcHostName)/httpAuth/app/rest/changes?locator=buildType:(id:$buildTypeId),sinceChange:(id:$($lastChange.id))"
	Write-Host $uri
	$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $tcUserName,$tcPassword)))

	$headers = @{ "Accept" = "application/json"; "Authorization" = "Basic {0}" -f $base64AuthInfo }

	$response = Invoke-RestMethod -Method Get -Uri $uri -Header $headers
	$diff = ""
	$response.change | ForEach{
		$output = Invoke-RestMethod -Method Get -Uri "$($tcHostName)$($_.href)"  -Header $headers 
		$diff = $diff + $output.comment
	}

	Write-Host $diff
	return $diff
}

function Fetch-LastSuccessfulVersion
{
	$uri = "{0}/httpAuth/app/rest/builds/?locator=buildType:id:{1},status:SUCCESS,count:1" -f $tcHostName,$buildTypeId
	$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $tcUserName,$tcPassword)))

	$headers = @{ "Accept" = "application/json"; "Authorization" = "Basic {0}" -f $base64AuthInfo }
	$response = Invoke-RestMethod -Method Get -Uri $uri -Header $headers
	return $response 
	
}

function Create-Jira($lastSuccessfultBuild, $commitMessages)
{
	$previousSuccessfulVersion = $lastSuccessfultBuild.build[0].number
	$previousSuccessfulBuildUrl = $lastSuccessfultBuild.build[0].webUrl
	$uri = "{0}/rest/api/2/issue/" -f $jiraHost
	$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $jiraUsername,$jiraPassword)))
	$headers = @{ "Accept" = "application/json"; "Content-Type" = "application/json"; "Authorization" = "Basic {0}" -f $base64AuthInfo }
	$body = @{
	    fields=@{
	    	project=@{
	    		key='SL'
	    	}
	    	summary="Release ticket - $component"
	    	description="h4. $component`r`nCurrent version:#$previousSuccessfulVersion `r`nNew version:#$newVersion`r`nh4. Rollback`r`nDeploy version [#$previousSuccessfulVersion|$previousSuccessfulBuildUrl]`r`nh4. Code changes`r`n$commitMessages`r`nh4. Related Stories`r`n$(Get-StoryTitlesByCommitMessages $commitMessages)`r`nh4. Impact statement`r`n$impactStatement"
	    	issuetype=@{
	    		name='Task'
	    	}

	    }
	}
	$json = $body | ConvertTo-Json
	$result = Invoke-RestMethod -Method Post -Uri $uri -Body $json -Header $headers 

	return $result.key
}

function Get-LastChange
{
	$uri = "$($tcHostName)/httpAuth/app/rest/builds?locator=buildType:(id:$buildTypeId),status:SUCCESS&fields=`$long,build(id,number,status,changes(`$long))"
	Write-Host $uri
	$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $tcUserName,$tcPassword)))

	$headers = @{ "Accept" = "application/json"; "Authorization" = "Basic {0}" -f $base64AuthInfo }

	$response = Invoke-RestMethod -Method Get -Uri $uri -Header $headers
	
	foreach($build in $response.build){
		if ($build.changes.count -eq "0"){
			continue
		}

		foreach($change in $build.changes.change){
			# First element is the last change since the result comes in descending order
			return $change
		}
	}

}

function Find-StoryIds($commitMessages)
{
	$stories = @()
	[regex]$regex = '#\d*'
	$regex.Matches($commitMessages) | 
	foreach-object {
		$stories += $_.Value.Substring(1)
	}
	return $stories
}

function Fetch-UserStory($storyId)
{
	$uri = "https://bat.tpondemand.com/api/v1/UserStories/{0}?access_token=NzM6dDVReGw1VSsrdHNoc0YrQy9UQkxTZUZ3ME1hR0NudThYbWZwMFByaVJIUT0=&include=[ID,Name,EntityState]" -f $storyId
	
	$headers = @{ "Accept" = "application/json" }
	$response 
	Try
	{
		$response = Invoke-RestMethod -Method Get -Uri $uri -Header $headers
	}
	Catch
	{
	return $null
	}
	return $response.Name
}
function Fetch-Bug($bugId)
{
	$uri = "https://bat.tpondemand.com/api/v1/Bugs/{0}?access_token={1}&include=[ID,Name,EntityState]" -f $bugId, $tpAccessToken
	
	$headers = @{ "Accept" = "application/json" }
	$response 
	Try
	{
		$response = Invoke-RestMethod -Method Get -Uri $uri -Header $headers
	}
	Catch
	{
		Write-Host "Returning null"
		return $null
	}
	return $response.Name
}
function Get-StoryTitlesByCommitMessages($commitMessage){
	$res = ""
	Find-StoryIds $commitMessage |
	foreach {
		$story = Fetch-UserStory $_
		if($story -eq $null){
			
			$bug = Fetch-Bug $_
			$res += $bug + "`r`n"
		}else{
			$res += $story + "`r`n"
		}
	}
	return $res
}

$lastSuccessfultBuild = Fetch-LastSuccessfulVersion
$commitMessages = Get-CommitMessages

$jiraId = Create-Jira $lastSuccessfultBuild $commitMessages
Write-Host "Created jira ticket https://livebookings.atlassian.net/browse/$jiraId"

# Local
# .\Create-Jira.ps1 -component "Test Feature" -newVersion "1.0.28" -buildTypeId "NeptuneUniverse_TestFeature_Production_51DeployToProduction" -tcUserName "payman.labbaf" -tcPassword "17ByronQ3" -tcHostName "http://172.20.0.103:8080" -jiraUsername "payman.labbaf@bookatable.com" -jiraPassword "17ByronQ3" -jiraProjectKey "SL" -jiraHost "https://livebookings.atlassian.net" -tpAccessToken "NzM6dDVReGw1VSsrdHNoc0YrQy9UQkxTZUZ3ME1hR0NudThYbWZwMFByaVJIUT0=" -impactStatement "Interruption of registration flow"
#
# TeamCity
# -component "%component%" -buildTypeId "NeptuneUniverse_TestFeature_Production_51DeployToProduction" -tcUserName "%teamcity.api.username%" -tcPassword "%teamcity.api.password%" -tcHostName "http://80.84.162.7:8080" -jiraUsername "%jira.api.username%" -jiraPassword "%jira.api.password%" -jiraProjectKey "SL" -newVersion "%env.BUILD_NUMBER%" -jiraHost "https://livebookings.atlassian.net" -tpAccessToken "NzM6dDVReGw1VSsrdHNoc0YrQy9UQkxTZUZ3ME1hR0NudThYbWZwMFByaVJIUT0=" -impactStatement "%impact.statement%"